import MemoryHandler from "./memory-handler";

const memoryHandlers = {
    linux: import("./linux/"),
    default: async () => null,
};

export const getMemoryHandler = async (): Promise<MemoryHandler | null> =>
    (await memoryHandlers[process.platform]) || (await memoryHandlers.default);
