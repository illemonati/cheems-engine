export default interface MemoryHandler {
    atoi: (s: string) => number;
    readMem: (pid: number) => void;
}
