import ffi from "ffi-napi";
import ref from "ref-napi";
import struct from "ref-struct-napi";

// const libc = ffi.Library("libc", {
//     printf: [ref.types.void, [ref.types.CString]],
// });

const iovec = struct({
    iov_base: ref.refType(ref.types.void),
    iov_len: ref.types.size_t,
});

const iovec2 = struct({
    iov_base: ref.types.uint32,
    iov_len: ref.types.size_t,
});

/**
 *        ssize_t process_vm_readv(pid_t pid,
                              const struct iovec *local_iov,
                              unsigned long liovcnt,
                              const struct iovec *remote_iov,
                              unsigned long riovcnt,
                              unsigned long flags);
 */
const lib = ffi.Library(null, {
    atoi: [ref.types.int, [ref.types.CString]],
    process_vm_readv: [
        ref.types.size_t,
        [
            ref.types.int,
            ref.refType(iovec),
            ref.types.long,
            ref.refType(iovec),
            ref.types.long,
            ref.types.long,
        ],
    ],
});

export const readMem = (pid: number) => {
    const buf = Buffer.alloc(1024);
    const localIov = new iovec({
        iov_base: buf.ref(),
        iov_len: 1024,
    });
    const liovcnt = 2;
    const remoteIov = new iovec2({
        iov_base: 173282403,
        iov_len: 1024,
    });
    const riovcnt = 1;
    const flags = 0;
    const a = lib.process_vm_readv(
        pid,
        localIov.ref(),
        liovcnt,
        remoteIov.ref(),
        riovcnt,
        flags
    );
    console.log(a);
    console.log(buf);
};

export const atoi = (s: string) => lib.atoi(s);
