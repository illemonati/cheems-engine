const nodeExternals = require("webpack-node-externals");

module.exports = {
    webpack: (config, { isServer }) => {
        if (!isServer) {
            config.target = "electron-renderer";
        }

        config.experiments = {
            ...config.experiments,
            topLevelAwait: true,
        };

        config.externals = [
            ...config.externals,
            nodeExternals({
                allowlist: [/^(?!(^(ffi-napi|ref-napi|ref-struct-napi)$)).*$/i],
            }),
        ];

        return config;
    },
};
